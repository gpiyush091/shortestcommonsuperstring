package fragments;

import java.util.ArrayList;
import java.util.Random;

public class FragmentsHandler {
	private ArrayList<String> seqFragments = new ArrayList<String>();
	private int fragmentLength = 0;
	private int numOfFragments = 0;
	
	public void setnumOfFragments(int len)
	{
		numOfFragments = len;
	}
	
	public int getnumOfFragments()
	{
		return numOfFragments;
	}
	
	public void setFragmentLength(int len)
	{
		fragmentLength = len;
	}
	
	public int getFragmentLength()
	{
		return fragmentLength;
	}
	
	public ArrayList<String> fragmentSequenceString(String str)
	{
		Random rand = new Random();
		//int numOfFragments = 5;//rand.nextInt(str.length()/2);
		for(int i=0;i<numOfFragments;i++){
			int beginIndex = rand.nextInt(str.length()-getFragmentLength()-1);
			seqFragments.add(str.substring(beginIndex, beginIndex+fragmentLength));
		}
		System.out.println("returning fragments");
		return seqFragments;
	}
}
