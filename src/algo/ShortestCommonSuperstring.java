package algo;

import java.util.ArrayList;

public class ShortestCommonSuperstring {
	private ArrayList<String> resultantString = new ArrayList<String>();
	
	private Boolean checkSubstring (String str1, String str2, int si1, int si2, int len)
	{
		return str1.regionMatches(si1, str2, si2, len);
	}

	public ArrayList<String> getShortestCommonSuperstring (ArrayList<String> fragmentList, int len)
	{
		int index=0;
		int index1=0;
		int seqLen = len;
		ArrayList<String> sequenceFragmentList = fragmentList;
		ArrayList<String> nextLevel = new ArrayList<String>();
		while(index1 < sequenceFragmentList.size()){
			String tmpString = sequenceFragmentList.get(index1);
			index = 0;
			while (index < sequenceFragmentList.size()){
				if (tmpString.equals(sequenceFragmentList.get(index))) {
					index++;
					continue;
				}else{
					ArrayList<String> tmpList = shortestCommonSuperstring(tmpString, sequenceFragmentList.get(index), seqLen);
					if (nextLevel.isEmpty()){
						nextLevel.addAll(tmpList);
					}else{
						nextLevel = copyShorterAndUniqueStrings(nextLevel, tmpList);
					}
					tmpList.clear();
				}
				index++;
			}
			index1++;
		}
		//Check whether all the strings have been covered or not
		ArrayList<String> fragList = sequenceFragmentList;
		nextLevel = recurseOnCheckFragmentsCovered(nextLevel, fragList, seqLen, sequenceFragmentList);
		return resultantString;
	}

	private ArrayList<String> recurseOnCheckFragmentsCovered(ArrayList<String> nextLevel, ArrayList<String> fragList, int seqLen, ArrayList<String> sequenceFragmentList)
	{
		ArrayList<String> notIncludedFragList = new ArrayList<String>();
		for (int r=0;r<nextLevel.size();r++){
			notIncludedFragList = checkAllFragmentsCovered(nextLevel.get(r), sequenceFragmentList);//fragList);
			if(!notIncludedFragList.isEmpty()) {
				fragList = notIncludedFragList;
				fragList.add(0,nextLevel.get(r));
				nextLevel = getFragments(fragList, seqLen);
				notIncludedFragList = recurseOnCheckFragmentsCovered(nextLevel, fragList, seqLen, sequenceFragmentList);
			} else {
				resultantString.add(nextLevel.get(r));
			}
		}
		return notIncludedFragList;
	}
	
	private ArrayList<String> getFragments(ArrayList<String> sequenceFragmentList, int seqLen)
	{
		int index = 1;
		String tmpString = sequenceFragmentList.get(0);
		ArrayList<String> nextLevel = new ArrayList<String>();
		while (index < sequenceFragmentList.size()){
			if (tmpString.equals(sequenceFragmentList.get(index))) {
				index++;
				continue;
			}else{
				ArrayList<String> tmpList = shortestCommonSuperstring(tmpString, sequenceFragmentList.get(index), seqLen);
				if (nextLevel.isEmpty()){
					nextLevel.addAll(tmpList);
				}else{
					nextLevel = copyShorterAndUniqueStrings(nextLevel, tmpList);
				}
				tmpList.clear();
			}
			index++;
		}
		return nextLevel; 
	}

	private ArrayList<String> checkAllFragmentsCovered(String superString, ArrayList<String> fragmentsList)
	{
		ArrayList<String> tmpList = new ArrayList<String>();
		tmpList.addAll(fragmentsList);
		for (int j=0;j<tmpList.size();j++){
			if ((superString.contains(tmpList.get(j)))) {
				tmpList.remove(j);
				j--;
			}
		}
		return tmpList;
	}

	private ArrayList<String> shortestCommonSuperstring (String str1, String str2, int seqLen)
	{
		ArrayList<String> tmpList = new ArrayList<String>();
		int index1=0;
		int len= 0;
		int index2=0;
		String tmpString = str1;
		index1 = 0;
		index2 = 0;
		if (str1.length()>str2.length()){
			len = str2.length();
		}else{
			len = str1.length();
		}
		int tmpLen = len;
		String s = tmpString;

		if ((str1.length()-str2.length())<0){
			index1 = 0;
			index2 = 0;
		}else{
			index1 = str1.length()-str2.length();
			index2 = 0;
		}

		//Get all strings in terms of suffixes
		while (len>=0){
			if (checkSubstring(tmpString, str2, index1, index2, len)){
				//Merge the 2 strings
				String str = tmpString.substring(0, str1.length() - len);
				str = str.concat(str2);
				tmpString = str;
				if (tmpList.isEmpty()){
					tmpList.add(tmpString);
				} else {
					substituteShorterString(tmpList, tmpString);
				}
				tmpString = s;
				break;
			}else{
				tmpString = tmpString.concat(str2);
				if (tmpList.isEmpty()){
					tmpList.add(tmpString);
				} else {
					substituteShorterString(tmpList, tmpString);
				}
				tmpString = s;
			}
			index1++;
			len--;
		}

		index1 = 0;
		index2 = 0;
		len = tmpLen;
		if ((str1.length()-str2.length())<0){
			index1 = 0;
			index2 = str2.length() - str1.length();
		}else{
			index1 = 0;
			index2 = 0;
		}
		tmpString = str1;
		String t = tmpString;
		while (len>=0){
			if (checkSubstring(tmpString, str2, index1, index2, len)) {
				//Merge the 2 strings
				String str = str2.substring(0, index2);
				str = str.concat(tmpString);
				tmpString = str;
				if (tmpList.isEmpty()){
					tmpList.add(tmpString);
				} else {
					substituteShorterString(tmpList, tmpString);
				}
				tmpString = t;
				break;
			}else{
				tmpString = str2.concat(tmpString);
				if (tmpList.isEmpty()){
					tmpList.add(tmpString);
				} else {
					substituteShorterString(tmpList, tmpString);
				}

				tmpString = t;
			}
			index2++;
			len--;
		}
		ArrayList<String> retList = removeDuplicatesAndLongerStrings(tmpList);
		return retList;
	}

	private ArrayList<String> removeDuplicatesAndLongerStrings(ArrayList<String> list)
	{
		int m=0;
		String tmpString = list.get(0);
		for (int i=0;i<list.size();i++){
			tmpString = list.get(i);
			m=i+1;
			while(m<list.size()){
				if(tmpString.equals(list.get(m))){
					list.remove(m);
					m--;
				}else if(tmpString.length()>list.get(m).length()){
					list.remove(i);
					m--;
				}
				m++;
			}	
		}
		return list;
	}

	private void substituteShorterString (ArrayList<String> tmpList, String tmpString)
	{
		int m=0;
		while (m<tmpList.size()){
			if (tmpList.get(m).length()>tmpString.length()) {
				tmpList.remove(m);
				tmpList.add(tmpString);
			}else if ((tmpList.get(m).length()==tmpString.length()) && (!(tmpList.get(m).equals(tmpString)))) {
				tmpList.add(tmpString);
			}
			m++;
		}
	}

	private ArrayList<String> copyShorterAndUniqueStrings (ArrayList<String> oldList, ArrayList<String> tmpList)
	{
		int m=0;
		int n=0;
		while(n<tmpList.size()){
			while (m<oldList.size()){
				if (oldList.get(m).length()>tmpList.get(n).length()) {
					oldList.remove(m);
					oldList.add(tmpList.get(n));
					break;
				}else if ((oldList.get(m).length()==tmpList.get(n).length()) && (!(oldList.get(m).equals(tmpList.get(n))))) {
					oldList.add(tmpList.get(n));
					break;
				}
				m++;
			}
			n++;
		}
		return oldList;
	}
}
