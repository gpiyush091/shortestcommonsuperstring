package main;

import java.io.File;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Timer;

import algo.ShortestCommonSuperstring;
import file.FileHandler;
import fragments.FragmentsHandler;

public class Main {
	public ArrayList<String> sequenceList = new ArrayList<String>();
	public int numOfSequences = 0;
	public ArrayList<String> seqFragmentList = new ArrayList<String>();
	
	public static void main (String[] args)
	{
		Main obj = new Main();
		obj.getInput();
		obj.getShortestCommonSuperstring();
		obj.sampleInput();
		obj.getShortestCommonSuperstring();
	}
	
	public void getInput()
	{
	
		seqFragmentList.clear();
		importTestSetfromFile();
		processSequences();
	}
	
	public void getShortestCommonSuperstring()
	{
		ShortestCommonSuperstring scs = new ShortestCommonSuperstring();
		long startTime = System.currentTimeMillis();
		ArrayList<String> tmpList = scs.getShortestCommonSuperstring(seqFragmentList,seqFragmentList.get(0).length());
		ArrayList<String> resultantStringList = removeDuplicatesAndLongerStrings(tmpList);
		printList(resultantStringList);
		long endTime = System.currentTimeMillis();
		System.out.println("Total time elasped in finding the superstring " + (endTime-startTime));
	}
	
	private ArrayList<String> removeDuplicatesAndLongerStrings(ArrayList<String> list)
	{
		int m=0;
		String tmpString = list.get(0);
		for (int i=0;i<list.size();i++){
			tmpString = list.get(i);
			m=i+1;
			while(m<list.size()){
				if(tmpString.equals(list.get(m))){
					list.remove(m);
					m--;
				}else if(tmpString.length()>list.get(m).length()){
					list.remove(i);
					m--;
				}
				m++;
			}	
		}
		return list;
	}

	private void sampleInput()
	{
		seqFragmentList.clear();
		seqFragmentList.add("abba");
		seqFragmentList.add("baab");
		seqFragmentList.add("baba");
		seqFragmentList.add("bbaa");
		//seqFragmentList.add("acctc");
		
	}
	
	private void processSequences()
	{
		FragmentsHandler fh = new FragmentsHandler();
		fh.setnumOfFragments(10);
		fh.setFragmentLength(5);
		for (int i=0;i<1;i++){
			seqFragmentList = fh.fragmentSequenceString(sequenceList.get(i));	
		}
		
		for (int i=0;i<seqFragmentList.size();i++)
		{
			System.out.println(seqFragmentList.get(i));
		}
	}
	
	private void importTestSetfromFile()
	{
		FileHandler fh = new FileHandler();
		String filePath = new File("resources\\testxx").getAbsolutePath();
		fh.fileReader(filePath);
		numOfSequences = fh.getNumWords();
		sequenceList = fh.getSequenceSet();
	}
	
	private void printList (ArrayList<String> list)
	{
		System.out.println("print list starts");
		int m=0;
		while (m<list.size()){
			System.out.println(list.get(m));
			m++;
		}
		System.out.println("print list ends");
	}
}
