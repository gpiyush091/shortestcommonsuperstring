package file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class FileHandler {
	private int numWords = 0;
	private ArrayList<String> sequenceList = new ArrayList<String>();
	
	private void setSequenceSet(ArrayList<String> seq)
	{
		sequenceList = seq;
	}
	
	public ArrayList<String> getSequenceSet()
	{
		return sequenceList;
	}
	
	private void setNumWords(int num)
	{
		numWords = num;
	}
	
	public int getNumWords()
	{
		return numWords;
	}
	
	public void fileReader(String filename)
	{
		ArrayList<String> sequences = new ArrayList<String>();
		BufferedReader reader=null;
		int count =0;
		try {
			String readline = null;
			reader = new BufferedReader(new FileReader(filename));
			while ((readline=reader.readLine())!=null){
				sequences.add(readline);
				count ++;
			}
			setNumWords(count);
			setSequenceSet(sequences);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader!=null){
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
